package eg.edu.alexu.csd.filestructure.sort.cs31;

import java.util.Collection;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

public class Heap<T extends Comparable<T>> implements IHeap<T> {

	INode<T> A[];
	int size=0;
	public INode<T> getRoot() {
		// TODO Auto-generated method stub
		return A[0];
	}

	
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	
	public void heapify(INode<T> node) {
		INode<T> l=node.getLeftChild();
		INode<T> r=node.getRightChild();
		INode<T> largest,temp;
		if((int)l.getValue()>(int)node.getValue()) {
			largest=l;
		}else {
			largest=node;
		}
		if((int)r.getValue()>(int)node.getValue()) {
			largest=r;
		}
		if(largest!=node) {
			temp=node;
			node=largest;
			largest=temp;
			heapify(largest);
		}
		
	}

	
	public T extract() {

		INode<T> max=A[1];
		A[1]=A[size];
		size--;
		heapify(A[1]);
		return max.getValue();
	}

	
	public void insert(T element) {
		INode<T> e = null;
		e.setValue(element);
		A[size]=e;
		heapify(A[1]);
		size++;
		
	}

	
	public void build(Collection<T> unordered) {
		size=unordered.size();
		for(int i=size/2;i>0;i--) {
			heapify(A[i]);
		}
		
	}

}
