package eg.edu.alexu.csd.filestructure.sort.cs31;

import eg.edu.alexu.csd.filestructure.sort.INode;

public class Node<T extends Comparable<T>> implements INode<T> {

	int index;
	T value;
	INode<T> A[];
	public INode<T> getLeftChild() {
		// TODO Auto-generated method stub
		return A[index*2];
		
	}


	public INode<T> getRightChild() {
		// TODO Auto-generated method stub
		return A[index*2+1];
	}


	public INode<T> getParent() {
		// TODO Auto-generated method stub
		return A[index/2];
		
	}


	public T getValue() {
		// TODO Auto-generated method stub
		return value;
	}


	public void setValue(T value) {
		this.value= value;
		
	}

}
