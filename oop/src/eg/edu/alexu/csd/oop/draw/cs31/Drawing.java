package eg.edu.alexu.csd.oop.draw.cs31;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;


import eg.edu.alexu.csd.oop.draw.DrawingEngine;
import eg.edu.alexu.csd.oop.draw.Shape;

public class Drawing implements DrawingEngine {
	
	List<Shape> allShapes=new ArrayList<>();
	Stack< Pair > undoStack=new Stack< Pair >();
	Stack< Pair> redoStack=new Stack< Pair>();
	Graphics c;
	@Override
	public void refresh(Graphics canvas) {
		// TODO Auto-generated method stub
		c=canvas;
		for(int i=0;i<allShapes.size();i++) {
			allShapes.get(i).draw(c);
		}
	}

	@Override
	public void addShape(Shape shape) {
		// TODO Auto-generated method stub
		allShapes.add(shape);
		undoStack.push(new Pair(true,shape));
		redoStack.clear();
		
	}

	@Override
	public void removeShape(Shape shape) {
		// TODO Auto-generated method stub
		allShapes.remove(shape);
		undoStack.push(new Pair(false,shape));
		redoStack.clear();
	}

	@Override
	public void updateShape(Shape oldShape, Shape newShape) {
		// TODO Auto-generated method stub
		for(int i=0;i<allShapes.size();i++) {
			if(allShapes.get(i)==oldShape) {
				allShapes.remove(i);
				allShapes.add(i, newShape);
				break;
			}
		}
		redoStack.clear();
	}

	@Override
	public Shape[] getShapes() {
		// TODO Auto-generated method stub
		int size=allShapes.size();
		Shape[] arr=new Shape[size];
		for(int i=0;i<size;i++) {
			arr[i]=allShapes.get(i);
		}
		return arr;
	}

	@Override
	public List<Class<? extends Shape>> getSupportedShapes() {
		// TODO Auto-generated method stub
		java.util.List<Class<? extends Shape>> list = new LinkedList<Class<? extends Shape>>();
		list.add(Circle.class);
		list.add(Ellipse.class);
		list.add(Line.class);
		list.add(Square.class);
		list.add(Rectangle.class);
		list.add(Triangle.class);
		
		String[] path = System.getProperty("java.class.path").split(""+File.pathSeparatorChar);
		try{
		for (int i=0;i<path.length;i++){
			if (!path[i].endsWith(".jar")) continue;
			JarInputStream jarFile = new JarInputStream(new FileInputStream(path[i]));
			JarEntry jarEntry;
			while ((jarEntry=jarFile.getNextJarEntry())!= null){
				if ((jarEntry.getName().endsWith(".class"))){
					String className = jarEntry.getName().replace('/','.').replaceAll(".class", "");
					Class<?> myclass=null;
					try{
						myclass = Class.forName(className);
					}catch(Throwable e){
						continue;
					}
					if (className.indexOf("$")!= -1) continue;
					int modifer = myclass.getModifiers();
					if (Modifier.isAbstract(modifer) ||
					Modifier.isInterface(modifer)) continue;
					Class<?>[] interfaces = myclass.getInterfaces();
					for (int k = 0; k < interfaces.length; k++) {
						if (interfaces[k].getName().equals("eg.edu.alexu.csd.oop.draw.Shape")){
							list.add((Class<? extends Shape>)myclass);
					        break; 	
					    }
					}
				}
			}
			jarFile.close();
		}
		}catch(Exception t){
			throw new RuntimeException ("Can not find plugins");
		}
		return list;

	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
		Shape sh=undoStack.peek().shape;
		boolean s=undoStack.peek().action;
		redoStack.push(new Pair(s,sh));
		if(s) {
			allShapes.remove(sh);
			
		}else if(!s) {
			allShapes.add(sh);
			redoStack.push(new Pair(s,sh));
		}
		undoStack.pop();

	}

	@Override
	public void redo() {
		// TODO Auto-generated method stub
		if(!redoStack.isEmpty()) {
		if(redoStack.peek().action==true) {
			//allShapes.add(redoStack.peek().shape);
			addShape(redoStack.peek().shape);
		}else if(redoStack.peek().action==false) {
			//allShapes.remove(undoStack.peek().shape);
			removeShape(redoStack.peek().shape);
		}
		//undoStack.push(redoStack.pop());
		}
		
		
		
	}

	@Override
	public void save(String path) {
		// TODO Auto-generated method stub

	}

	@Override
	public void load(String path) {
		// TODO Auto-generated method stub

	}

}
