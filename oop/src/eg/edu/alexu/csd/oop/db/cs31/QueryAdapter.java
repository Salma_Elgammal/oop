package eg.edu.alexu.csd.oop.db.cs31;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryAdapter {

	static String where;
	static String whereValue;
	static char oper;
	static ArrayList<String> select;
	static ArrayList<String> updateValue;
	static ArrayList<String> field;
	static ArrayList<String> Type;

	static String table;
	static String database;
	static Pattern p;
	static Matcher m;
	static ExcuteQuery ex;
	static ArrayList<String> insertValue;
	static ArrayList<Boolean> iNtype;
	static ArrayList<FieldType> someFields;

	private QueryAdapter() {
		ex = new ExcuteQuery();
		where = null;
		select = new ArrayList<>();
		field = new ArrayList<>();
		Type = new ArrayList<>();
		iNtype = new ArrayList<>();
		someFields = new ArrayList<>();
		updateValue = new ArrayList<>();
		insertValue = new ArrayList<>();
		table = null;
		database = null;

	}

	public static boolean createQuery(String query) {
		if (query == null) {
			return false;
		}
		p = Pattern.compile("^(CREATE\\s*DATABASE)\\s+(\\w+)\\s*;?$", Pattern.CASE_INSENSITIVE);
		m = p.matcher(query);
		if (m.matches()) {
			database = m.group(2).replaceAll("\\s+", "");
			return ex.createDatabase(database);
		} else {
			p = Pattern.compile("^(CREATE\\s*\\s*TABLE|create\\s*\\s*table)\\s+(\\w+)\\s*\\(("
					+ "((\\s*(\\w+\\s*)+\\s+(int|INT|varchar\\s*\\(\\d+\\s*\\)\\s*|VARCHAR\\s*\\(\\d+\\s*\\)\\s*)),)*"
					+ "\\s*(\\w+\\s*)+\\s+(int|INT|varchar\\s*\\(\\d+\\s*\\)|VARCHAR\\s*\\(\\d+\\s*\\))\\s*)\\)"
					+ "\\s*;?$", Pattern.CASE_INSENSITIVE);
			m = p.matcher(query);
			if (m.matches()) {
				table = m.group(2).replaceAll("//s+", "");
				getTypeField(m.group(3));
				return ex.createTable(table, field, Type);
			}
		}
		return false;
	}

	private static void getTypeField(String group) {

		p = Pattern.compile("^\\s*((\\w+\\s*)+)\\s+(int|INT|varchar\\s*\\(\\d+\\s*\\)|VARCHAR\\s*\\(\\d+\\s*\\))\\s*$",
				Pattern.CASE_INSENSITIVE);
		field.clear();
		Type.clear();
		String[] str = group.split(",");
		for (String s1 : str) {
			Matcher m2 = p.matcher(s1);
			if (m2.matches()) {
				field.add(m2.group(1));
				Type.add(m2.group(3).replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("255", ""));
			}
		}

	}

	public static boolean dropQuery(String query) {
		if (query == null) {
			return false;
		}
		p = Pattern.compile("^(DROP\\s*DATABASE|drop\\s*database)\\s+(\\w+)\\s*;?$", Pattern.CASE_INSENSITIVE);
		m = p.matcher(query);

		if (m.matches()) {
			database = m.group(2).replaceAll("\\s+", "");
			return ex.dropDataBase(database);
		} else {
			p = Pattern.compile("^(DROP\\s*TABLE|drop\\s*table)\\s+(\\w+)\\s*;?$", Pattern.CASE_INSENSITIVE);
			m = p.matcher(query);

			if (m.matches()) {
				table = m.group(2).replaceAll("//s+", "");
				return ex.dropTable(table);
			}
		}
		return false;
	}

	public static List<List<Object>> selectQuery(String query) {
		if (query == null) {
			return new ArrayList<>();
		}
		List<List<Object>> list = new ArrayList<>();
		p = Pattern.compile("^(SELECT|select)\\s\\*\\s(FROM|from)\\s(\\w+)\\s*;?$", Pattern.CASE_INSENSITIVE);
		m = p.matcher(query);
		if (m.matches()) {
			list = ex.selectAll(m.group(3).trim());
		} else {
			p = Pattern.compile(
					"^(SELECT|select)\\s+((\\s*[a-zA-Z0-9_\\s]+\\s*\\,)*\\s*[a-zA-Z0-9_\\s]+)\\s+(FROM|from)(\\s+(\\w+))\\s*;?$",
					Pattern.CASE_INSENSITIVE);
			m = p.matcher(query);
			if (m.matches()) {
				table = m.group(5).trim();
				getFields(m.group(2));
				list = ex.selectDistinct(table, select);
			} else {
				p = Pattern.compile(
						"^(SELECT|select)\\s+" + "((\\s*[a-zA-Z0-9_\\s]+\\s*\\,)*\\s*[a-zA-Z0-9_\\s]+)\\s+"
								+ "(FROM|from)\\s+(\\w+)\\s+"
								+ "(WHERE|where)\\s+((\\w+)\\s*(=|>|<)\\s*(('(\\w+\\s*.*)+')|(\\d+)))\\s*;?$",
						Pattern.CASE_INSENSITIVE);
				/*
				 * p = Pattern.compile( "^(SELECT|select)\\s+" +
				 * "((\\s*(\\w+\\s*)+\\s*\\,)*\\s*(\\w+\\s*)+)\\s+" +
				 * "(FROM|from)\\s+(\\w+)\\s+" +
				 * "(WHERE|where)\\s+((\\w+)\\s*(=|>|<)\\s*(('(\\w+\\s*.*)+')|(\\d+)))\\s*$",
				 * Pattern.CASE_INSENSITIVE);
				 */
				m = p.matcher(query);
				if (m.matches()) {
					table = m.group(5).replaceAll("\\s+", "");
					getFields(m.group(2));
					getCondition(m.group(7));
					list = ex.selectDisWithCondition(table, select, where, whereValue, oper);
				} else {
					p = Pattern.compile(
							"^(SELECT|select)\\s\\*\\s(FROM|from)\\s(\\w+)\\s+(WHERE|where)\\s+((\\w+)\\s*(=|>|<)\\s*(('(\\w+\\s*.*)+')|(\\d+)))\\s*;?$",
							Pattern.CASE_INSENSITIVE);
					m = p.matcher(query);
					if (m.matches()) {
						table = m.group(3).replaceAll("\\s+", "");
						if (getCondition(m.group(5))) {
							list = ex.selectAllWithCondition(table, where, oper, whereValue);
						}
					}
				}
			}
		}
		return list;
	}

	private static void getFields(String query) {
		String[] t = query.split(",");
		select.clear();
		p = Pattern.compile("\\s*(\\w+\\s*)*\\s*");
		for (String x : t) {
			Matcher m2 = p.matcher(x);
			if (m2.matches())
				select.add(x.trim());
		}
	}

	private static boolean getCondition(String group) {
		String[] temp = null;
		if (group.contains("=")) {
			temp = group.split("=");
			oper = '=';
		} else if (group.contains(">")) {
			temp = group.split(">");
			oper = '>';

		} else if (group.contains("<")) {
			temp = group.split("<");
			oper = '<';

		}
		where = temp[0].trim();
		if (!ex.ifExist(where, table)) {
			return false;
		}
		if (!temp[1].contains("'")) {
			if (!ex.checkInteger(where, table)) {
				return false;
			}
		}
		whereValue = temp[1].replaceAll("'", "").trim();
		return true;

	}

	public static int insertQuery(String query) {
		if (query == null) {
			return 0;
		}
		p = Pattern.compile("^(INSERT INTO|insert into)\\s+" + "(\\w+)\\s+" + "(VALUES|values)\\s*"
				+ "\\(((\\s*(('[a-zA-Z0-9_\\s_.]+')|(\\d+))\\s*\\,)*\\s*(('[a-zA-Z0-9_\\s]+')|(\\d+)))\\)" + "\\s*;?$",
				Pattern.CASE_INSENSITIVE);
		m = p.matcher(query);
		if (m.matches()) {
			table = m.group(2).trim();
			if (ex.checkTableExist(table)) {
				getInsertValues(m.group(4));
				if (checkAllFieldsType(table)) {
					return ex.insertInAll(table, insertValue);
				} else {
					return 0;
				}
			}
		} else {
			p = Pattern.compile(
					"^(INSERT INTO|insert into)\\s+" + "(\\w+)\\s*" + "\\(((\\s*\\w+\\s*\\,)*\\s*(\\w+))\\)\\s*"
							+ "(VALUES|values)\\s*"
							+ "\\(((\\s*(('[a-zA-Z0-9_\\s_.]+')|(\\d+))\\s*\\,)*\\s*(('[a-zA-Z0-9_\\s]+')|(\\d+)))\\)\\s*;?$",
					Pattern.CASE_INSENSITIVE);

			m = p.matcher(query);
			if (m.matches()) {
				table = m.group(2).trim();
				if (ex.checkTableExist(table)) {
					getInsertValues(m.group(7));
					getFields(m.group(3));
					if (checkSomeFieldsType(select, table)) {
						return ex.insertInDist(table, someFields, insertValue, Type);

					}

				}
			}
		}
		return 0;
	}

	private static boolean checkSomeFieldsType(ArrayList<String> select2, String table2) {
		if (!fieldsExist(table2, select2)) {
			return false;
		}
		for (int i = 0; i < iNtype.size(); i++) {
			if (iNtype.get(i)) {
				if (!someFields.get(i).isInteger()) {
					return false;
				}
			} else {
				if (!someFields.get(i).isString()) {
					return false;
				}
			}

		}
		return true;
	}

	private static boolean fieldsExist(String table2, ArrayList<String> select2) {
		ArrayList<FieldType> fields = ex.getFields(table2);
		someFields.clear();
		for (FieldType ft : fields) {
			for (String f : select2) {
				if (f.equalsIgnoreCase(ft.getName())) {
					someFields.add(ft);
				}
			}
		}
		if (someFields.size() != select2.size()) {
			return false;
		}
		return true;
	}

	private static boolean checkAllFieldsType(String table) {
		ArrayList<FieldType> fields = ex.getFields(table);
		for (int i = 0; i < iNtype.size(); i++) {
			if (iNtype.get(i)) {
				if (!fields.get(i).isInteger()) {
					return false;
				}
			} else {
				if (!fields.get(i).isString()) {
					return false;
				}
			}

		}
		return true;

	}

	private static void getInsertValues(String group) {
		String[] t = group.split(",");
		insertValue.clear();
		iNtype.clear();
		for (String x : t) {
			if (x.contains("'")) {
				insertValue.add(x.replaceAll("'", "").trim());
				iNtype.add(false);
			} else {
				insertValue.add(x.trim());
				iNtype.add(true);
			}

		}

	}

	public static int deleteQuery(String query) {
		if (query == null) {
			return 0;
		}
		p = Pattern.compile(
				"^(DELETE FROM|delete  from)\\s+(\\w+)\\s+(WHERE|where)\\s+(((\\w+)\\s*)+(=|>|<)\\s*'((\\w+)\\s*)+')\\s*;?$",
				Pattern.CASE_INSENSITIVE);
		m = p.matcher(query);
		if (m.matches()) {
			table = m.group(2).trim();
			if (ex.checkTableExist(table)) {
				if (getCondition(m.group(4))) {
					return ex.deleteFromTableWithCondition(table, oper, where, whereValue);
				}
			}
		} else {
			p = Pattern.compile("^(DELETE(\\*)*\\s+FROM|)\\s+(\\w+)\\s*$", Pattern.CASE_INSENSITIVE);
			m = p.matcher(query);
			if (m.matches()) {
				table = m.group(3).trim();
				if (ex.checkTableExist(table)) {
					return ex.deleteFromTable(table);
				}
			}
		}
		return 0;
	}

	public static int updateQuery(String query) {
		if (query == null) {
			return 0;
		}
		p = Pattern.compile(
				"^(UPDATE)\\s+(\\w+)\\s+(SET)\\s+(((\\w+)\\s*=\\s*(('[a-zA-Z0-9_\\s_.]+')|(\\d+))\\s*\\,)*(\\s*(\\w+)\\s*=\\s*(('[a-zA-Z0-9_\\s_.]+')|(\\d+))))\\s*;?$",
				Pattern.CASE_INSENSITIVE);
		m = p.matcher(query);

		if (m.matches()) {
			table = m.group(2).trim();
			if (ex.checkTableExist(table)) {
				getUpdate(m.group(4));
				return ex.updateTable(table, updateValue, someFields);
			}
		} else {
			p = Pattern.compile(
					"^(UPDATE|update)\\s+(\\w+)\\s+(SET|set)\\s+(((\\w+)\\s*=\\s*(('[a-zA-Z0-9_\\s_.]+')|(\\d+))\\s*\\,)*(\\s*(\\w+)\\s*=\\s*(('[a-zA-Z0-9_\\s_.]+')|(\\d+))))\\s+(WHERE|where)\\s+((\\w+)\\s*(=|>|<)\\s*(\\w+))\\s*;?$");
			m = p.matcher(query);

			if (m.matches()) {
				table = m.group(2).trim();
				getUpdate(m.group(4));
				getCondition(m.group(16));
				return ex.updateTableWithCondition(table, updateValue, someFields, where, whereValue, oper);
			}
		}
		return 0;
	}

	private static boolean getUpdate(String group) {
		String[] t = group.split(",");
		ArrayList<String> f = new ArrayList<>();
		ArrayList<String> v = new ArrayList<>();
		updateValue.clear();

		for (String x : t) {
			String[] temp = x.split("=");
			f.add(temp[0].trim());
			v.add(temp[1].trim());
		}
		if (!fieldsExist(table, f)) {
			return false;
		}
		if (f.size() != v.size()) {
			return false;
		}
		for (FieldType ft : someFields) {
			for (String ft1 : f) {
				if (ft1.equals(ft.getName())) {
					updateValue.add(v.get(f.indexOf(ft1)));
				}
			}
		}

		for (int i = 0; i < updateValue.size(); i++) {
			if (!updateValue.get(i).contains("'")) {
				if (!someFields.get(i).isInteger()) {
					return false;
				}
			} else {
				if (!someFields.get(i).isString()) {
					return false;
				}
				updateValue.set(i, updateValue.get(i).replaceAll("'", ""));
			}

		}

		return true;

	}

}