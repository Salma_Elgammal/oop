package eg.edu.alexu.csd.oop.db.cs31;

import java.sql.SQLException;

public class ResultSetMetaData implements java.sql.ResultSetMetaData {

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public String getCatalogName(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public String getColumnClassName(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public int getColumnCount() throws SQLException {
		//code
		return 0;
	}

	public int getColumnDisplaySize(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public String getColumnLabel(int column) throws SQLException {
		//code
		return null;
	}

	public String getColumnName(int column) throws SQLException {
		//code
		return null;
	}

	public int getColumnType(int column) throws SQLException {
		//code
		return 0;
	}

	public String getColumnTypeName(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public int getPrecision(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public int getScale(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public String getSchemaName(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public String getTableName(int column) throws SQLException {
		//code
		return null;
	}

	public boolean isAutoIncrement(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public boolean isCaseSensitive(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public boolean isCurrency(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public boolean isDefinitelyWritable(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public int isNullable(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public boolean isReadOnly(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public boolean isSearchable(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public boolean isSigned(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	public boolean isWritable(int column) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

}