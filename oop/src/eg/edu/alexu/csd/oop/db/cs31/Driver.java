package eg.edu.alexu.csd.oop.db.cs31;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class Driver implements java.sql.Driver{
	
	Properties properties = new Properties();

	public boolean acceptsURL(String url) throws SQLException {
		//code
		String[] s=url.split(":");
		if(s[0].equals("jdbc") && s[1].equals("xmldb") && s[2].equals("//localhost")) {
			return true;
		}
		return false;
	}

	public Connection connect(String url, Properties info) throws SQLException {
		//code
		File dir = (File) info.get("path");
	    String path = dir.getAbsolutePath();
	    return DriverManager.getConnection(path); // pool
	}

	public int getMajorVersion() {
		throw new java.lang.UnsupportedOperationException();
	}

	public int getMinorVersion() {
		throw new java.lang.UnsupportedOperationException();
	}

	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		throw new java.lang.UnsupportedOperationException();
	}

	public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
		//code
		return null;
	}

	public boolean jdbcCompliant() {
		throw new java.lang.UnsupportedOperationException();
	}


}