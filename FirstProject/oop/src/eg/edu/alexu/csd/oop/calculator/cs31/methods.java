package eg.edu.alexu.csd.oop.calculator.cs31;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import eg.edu.alexu.csd.oop.calculator.Calculator;

public class methods implements Calculator {
	String current;
	ArrayList<String> arr=new ArrayList<String>();
	int index=-1,savedIndex=0;
	File file = new File("file.txt");
	
	public void input(String s) {
		
		// TODO Auto-generated method stub
				this.current=s;
				if(index>=4) {
					arr.remove(0);
					arr.add(current);

				}else {
					arr.add(current);
				}
				index = arr.size()-1;
	}

	public String getResult() {
		// TODO Auto-generated method stub
		double res=0,first=0,second=0;
		char operator;
		String result;
		int i=0;
		int flag=1;
		while(current.charAt(i)!='+' && current.charAt(i)!='*' && current.charAt(i)!='/' && current.charAt(i)!='-' ){
			if(current.charAt(i)=='.' || flag!=1){
				if(flag!=1) {
					flag=flag*10;
					first=first+((double)Math.abs(current.charAt(i)-'0'))/flag;
				}else {
					i++;
					flag=flag*10;
					first=first+((double)Math.abs(current.charAt(i)-'0'))/flag;
				}
			}else{
				first=(first*10)+(current.charAt(i)-'0');
			}
			i++;
		}
		operator=current.charAt(i);
		i++;
		flag=1;
		while(i!=current.length()){
			if(current.charAt(i)=='.' || flag!=1){
				if(flag!=1) {
					flag=flag*10;
					second=second+((double)Math.abs(current.charAt(i)-'0'))/flag;
				}else {
					i++;
					flag=flag*10;
					second=second+((double)Math.abs(current.charAt(i)-'0'))/flag;
				}
			}else{
				second=(second*10)+(current.charAt(i)-'0');
			}
			i++;
		}
		
		if(operator=='+'){
			res=first+second;
		}else if(operator=='-'){
			res=first-second;
		}else if(operator=='*'){
			res=first*second;
		}else{
			res=first/second;
		}
		result=String.valueOf(res);
		
		return result;
	}

	public String current() {
		// TODO Auto-generated method stub
		if(index==-1) {
			return null;
		}
		current=arr.get(index);
		return current;
	}

	public String prev() {
		// TODO Auto-generated method stub
		
		if(index>0){
			index--;
			current=arr.get(index);
			return current;
			
		}
		return null;
	}

	public String next() {
		// TODO Auto-generated method stub
		if(index<arr.size()-1){
			index++;
			current=arr.get(index);
			return current;
		}
		return null;
	}

	public void save() {
		// TODO Auto-generated method stub
		 try{
			 clearTheFile();
	            file.createNewFile();
	            FileWriter fw = new FileWriter(file);
	            BufferedWriter bw = new BufferedWriter(fw);
	            int size=arr.size();
	            String s=String.valueOf(index);
	            bw.write(s+"\n");
	            int i=0;
	            while(i<size){
	            	bw.write(arr.get(i)+"\n");
	            	i++;
	            }
	            bw.flush();
	            bw.close();

	        }catch(IOException e){
	        e.printStackTrace();
	        }
	}

	public void load(){
		// TODO Auto-generated method stub
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("file.txt"));
			arr.clear();
			String ind=br.readLine();
			if(ind==null) {
				index=-1;
				return;
			}
			index=ind.charAt(0)-'0';
			String temp = br.readLine();
		    while (temp != null) {
		        arr.add(temp);

		        temp = br.readLine();
		      
		    }
		    br.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	
	
	}
	private void clearTheFile() {
        FileWriter fwOb = null;
		try {
			fwOb = new FileWriter("formulas.txt", false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter pwOb = new PrintWriter(fwOb, false);
        pwOb.flush();
        pwOb.close();
        try {
			fwOb.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }



}